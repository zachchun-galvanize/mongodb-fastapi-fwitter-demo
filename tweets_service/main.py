from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
import os
from pydantic import BaseModel
from tweet_queries import TweetQueries

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class UserIn(BaseModel):
    email: str

class TweetIn(BaseModel):
    text: str
    user: UserIn


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "year": 2022,
            "month": 12,
            "day": "9",
            "hour": 19,
            "min": 0,
            "tz:": "PST"
        }
    }



@app.post("/api/tweets")
def create_tweet(
    tweet: TweetIn,
    queries: TweetQueries = Depends(),
):
    return queries.create_tweet(tweet)

@app.get("/api/tweets/{id}")
def get_tweet(
    id: str,
    queries: TweetQueries = Depends(),
):
    return queries.get_tweet_by_id(id)
