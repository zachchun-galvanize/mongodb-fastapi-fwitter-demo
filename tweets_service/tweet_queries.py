import pymongo
import os
from bson import ObjectId
# fix ObjectId & FastApi conflict
import pydantic
pydantic.json.ENCODERS_BY_TYPE[ObjectId]=str
client = pymongo.MongoClient(os.environ.get('DATABASE_URL'))
db = client['fwitter']
collection = db['tweets'] # collection == table, the thing you query for data


class TweetQueries:
    def get_tweet_by_id(self, id):
        result = collection.find_one({ '_id': ObjectId(id) })
        return result
    
    def create_tweet(self, tweet):
        # 	- what type is it currently? 
	    # how can we convert the data to the type we need?
        result = collection.insert_one(tweet.dict())
        return result.inserted_id